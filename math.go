package vulcan_weather

import (
	"github.com/spf13/viper"
	_ "gitlab.com/javiern1/vulcan_weather/config"
	"math"
)

var COLLINEAR_THRESHOLD float64

func init() {
	COLLINEAR_THRESHOLD = viper.GetFloat64("COLLINEAR_THRESHOLD")
}

type Point struct {
	X float64
	Y float64
}

type PolarPosition struct {
	Angle  float64
	Radius float64
}

func DegToRad(degrees float64) float64 {
	return degrees * math.Pi / 180
}

func PolarToCartesian(position PolarPosition) Point {
	return Point{
		X: position.Radius * math.Cos(position.Angle),
		Y: position.Radius * math.Sin(position.Angle),
	}
}

// this check if 3 points are collinear. due to float-point limitations, we cant get an exactly if they are collinear,
// so we have to make an approximation. the idea is, take 2 of the points, make a line, and measure the distance to the third point
// if it is between the threshold, then we consider the points alligned
func Collinear(a, b, c Point) bool {

	//fmt.Printf("Points: \n A: %+v\n B: %+v\n C: %+v\n", a, b, c)

	slopeAB := slope(a, b)
	DAB := distanceToLine(slopeAB, intercept(a, slopeAB), c)

	slopeAC := slope(a, c)
	DAC := distanceToLine(slopeAC, intercept(a, slopeAC), b)

	slopeBC := slope(b, c)
	DBC := distanceToLine(slopeAB, intercept(b, slopeBC), a)

	//fmt.Printf("Distance: \n AB: %+v\n AC: %+v\n BC: %+v\n", DAB, DAC, DBC)

	return (DAB < COLLINEAR_THRESHOLD || DAC < COLLINEAR_THRESHOLD || DBC < COLLINEAR_THRESHOLD)
}

func Perimeter(a, b, c Point) float64 {
	return distanceToPoint(a, b) + distanceToPoint(a, c) + distanceToPoint(b, c)
}

// this check if p is inside the triangle defined by a b and c
// based on this: https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
// and this: https://math.stackexchange.com/questions/51326/determining-if-an-arbitrary-point-lies-inside-a-triangle-defined-by-three-points
func CheckPointInsideTriangle(a, b, c, p Point) bool {
	b1 := sign(p, a, b) < 0.0
	b2 := sign(p, b, c) < 0.0
	b3 := sign(p, c, a) < 0.0

	return (b1 == b2) && (b1== b3)
}

func sign(a, b, c Point) float64 {
	return (a.X-c.X)*(b.Y-c.Y) - (b.X-c.X)*(a.Y-c.Y)
}

func slope(a, b Point) float64 {
	return (b.Y - a.Y) / (b.X - a.X)
}

func intercept(a Point, slope float64) float64 {
	return a.Y - slope*a.X
}

func distanceToLine(slope, intercept float64, a Point) float64 {
	return (math.Abs(slope*a.X - a.Y + intercept)) / math.Sqrt(math.Pow(slope, 2)+1)
}

func distanceToPoint(a, b Point) float64 {
	return math.Sqrt(math.Pow(a.X-b.X, 2) + math.Pow(a.Y-b.Y, 2))
}
