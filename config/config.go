package config

import (
	"github.com/spf13/viper"
	"strings"
)

func init() {
	viper.SetDefault("PORT", ":8080")
	viper.SetDefault("COLLINEAR_THRESHOLD", 30.0)
	viper.SetDefault("DAYS_IN_YEAR", 360)
	viper.SetDefault("YEARS", 10)

	// connection string format: USER:PASSWORD@unix(/cloudsql/PROJECT_ID:REGION_ID:INSTANCE_ID)/[DB_NAME]
	//viper.SetDefault("DSN", "root:weather@unix(/cloudsql/meli-251021:us-east1:weather)/weather")
	viper.SetDefault("DSN", "root:weather@tcp(localhost:3306)/weather")

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	// from a config file
	viper.SetConfigName("config")
	viper.AddConfigPath("./")
	viper.AddConfigPath("$HOME/.weather")

	// NOTE: this will require that you have config file somewhere in the paths specified. It can be reading from JSON, TOML, YAML, HCL, and Java properties files.
	viper.ReadInConfig()
}
