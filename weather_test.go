package vulcan_weather

import "testing"

func TestWeatherForDay0(t *testing.T) {
	// in day 0 , all planets are over the X axis, this means, all has Y=0,
	// which means they are all aligned, and aligned with the sun, so, it has to be a drought day
	Weather, _ := WeatherFor(0)

	if Weather != Drought {
		t.Error("This day must be Drought")
	}

}

func TestWeatherForDay110(t *testing.T) {
	// in day 0 , all planets are over the X axis, this means, all has Y=0,
	// which means they are all aligned, and aligned with the sun, so, it has to be a drought day
	Weather, _ := WeatherFor(110)

	if Weather != Rainy {
		t.Error("This day must be Rainy")
	}

}

func TestWeatherForDay125(t *testing.T) {
	// in day 0 , all planets are over the X axis, this means, all has Y=0,
	// which means they are all aligned, and aligned with the sun, so, it has to be a drought day
	Weather, _ := WeatherFor(125)

	if Weather != Other {
		t.Error("This day must be Other")
	}

}