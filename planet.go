package vulcan_weather

// this is used to track orbit direction

// ORBITCW: ClockWise
// ORBITCW: CounterClockWise

const (
	ORBITCW  int = 1
	ORBITCCW int = -1
)

// Struct to hold data about a planet
// the speed must expressed in radians
// distance to sun is in kilometers
type Planet struct {
	Name          string
	Direction     int
	Speed         int
	DistanceToSun float64
}

// initializes a new planet
func New(name string, direction int, speed int, distanceToSun float64) *Planet {
	return &Planet{
		Name:          name,
		Direction:     direction,
		Speed:         speed,
		DistanceToSun: distanceToSun,
	}
}

// calculates planet position based on the day number
func (planet *Planet) GetRadPosition(day int) PolarPosition {

	angle := (planet.Speed * day) % 360

	if planet.Direction == ORBITCW && angle != 0 {
		angle = 360 - angle
	}

	return PolarPosition{
		Angle:  DegToRad(float64(angle)),
		Radius: planet.DistanceToSun,
	}
}

func (planet *Planet) GetDegPosition(day int) Point {
	return PolarToCartesian(planet.GetRadPosition(day))
}
