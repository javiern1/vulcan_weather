package vulcan_weather

type Weather int

type Prediction struct {
	Drought   int64 `json:"drought"`
	Rainy     int64 `json:"rainy"`
	RainPeaks []int `json:"rain_peaks"`
	Optimal   int64 `json:"optimal"`
}

const (
	Drought Weather = iota
	Rainy
	Optimal
	Other
)

var Planets = map[string]*Planet{
	"Ferengi": &Planet{
		Name:          "Ferengi",
		Direction:     ORBITCW,
		Speed:         1,
		DistanceToSun: 500,
	},
	"Betasoide": &Planet{
		Name:          "Betasoide",
		Direction:     ORBITCW,
		Speed:         3,
		DistanceToSun: 2000,
	},
	"Vulcano": &Planet{
		Name:          "Vulcano",
		Direction:     ORBITCCW,
		Speed:         5,
		DistanceToSun: 1000,
	},
}


func Predict(days int) (*Prediction) {
	p := &Prediction{}

	prevWeather, perimeter := WeatherFor(0)

	p.incrementPeriodCounter(prevWeather)

	p.findRainPeak(0, prevWeather, perimeter)

	for x := 1; x < days; x++ {

		weather, perimeter := WeatherFor(x)

		p.findRainPeak(x, weather, perimeter)

		if prevWeather != weather {
			//weather changed, need to increment period counter
			// and change previous type
			p.incrementPeriodCounter(weather)
			prevWeather = weather
		}

	}

	return p
}

func (p * Prediction) incrementPeriodCounter(w Weather) {
	switch w {
	case Drought:
		p.Drought++
	case Rainy:
		p.Rainy++
	case Optimal:
		p.Optimal++
	}
}

var maxPerimeter float64
func (p * Prediction) findRainPeak(day int, weather Weather, perimeter float64) {
	if weather == Rainy {
		if perimeter < maxPerimeter {
			return
		}
		if perimeter > maxPerimeter {
			//new max found, so we have to reset the max days
			p.RainPeaks = nil
			maxPerimeter = perimeter
		}
		p.RainPeaks = append(p.RainPeaks, day)
	}
}


func WeatherFor(day int) (Weather, float64) {
	//obtengo las coordenadas de los planetas y valido si estan alineados
	pFerengi := Planets["Ferengi"].GetDegPosition(day)
	pBetasoide := Planets["Betasoide"].GetDegPosition(day)
	pVulcano := Planets["Vulcano"].GetDegPosition(day)
	arePlanetsAligned := Collinear(pFerengi, pBetasoide, pVulcano)
	weather := Other

	if arePlanetsAligned {
		//if they are alligned, i check if they are alligned with sun also
		arePlanetsAlignedWithSun := Collinear(pFerengi, pBetasoide, Point{X: 0, Y: 0})
		if arePlanetsAlignedWithSun {
			weather = Drought
		} else {
			weather = Optimal
		}
	} else {
		// check if sun is inside the triangule
		if CheckPointInsideTriangle(pFerengi, pBetasoide, pVulcano, Point{X: 0, Y: 0}) {
			weather = Rainy
		}
	}

	return weather, Perimeter(pFerengi, pBetasoide, pVulcano)
}
