module gitlab.com/javiern1/vulcan_weather

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/pkg/errors v0.8.0
	github.com/spf13/viper v1.4.0
	golang.org/x/tools v0.0.0-20190311212946-11955173bddd
)
