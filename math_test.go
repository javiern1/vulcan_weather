package vulcan_weather

import "testing"

func TestCollinearWithCollinearPoints(t *testing.T) {
	a := Point{X: 100, Y: 100}
	b := Point{X: 200, Y: 200}
	c := Point{X: 300, Y: 300}

	result := Collinear(a, b, c)

	if !result {
		t.Errorf("Error asserting than the points where collinear")
	}
}

func TestCollinearWithNotCollinearPoints(t *testing.T) {
	a := Point{X: 100, Y: 100}
	b := Point{X: 200, Y: 200}
	c := Point{X: 300, Y: -300}

	result := Collinear(a, b, c)

	if result {
		t.Errorf("Error asserting than the points where not collinear")
	}
}

func TestCollinearWithNotCollinearButCloseEnoughPoints(t *testing.T) {
	a := Point{X: 10, Y: 10}
	b := Point{X: 20, Y: 20}
	c := Point{X: 30, Y: -20}

	result := Collinear(a, b, c)

	if !result {
		t.Errorf("Error asserting than the points where collinear")
	}
}

func TestCheckPointInsideTriangle(t *testing.T) {
	a := Point{X: 0, Y: 2}
	b := Point{X: -2, Y: -1}
	c := Point{X: 2, Y: -1}
	p := Point{X: 0, Y: 0}

	result := CheckPointInsideTriangle(a, b, c, p)

	if !result {
		t.Errorf("Error asserting than the point is inside the triangle")
	}
}

func TestCheckPointOutsideTriangle(t *testing.T) {
	a := Point{X: 0, Y: 2}
	b := Point{X: -2, Y: 1}
	c := Point{X: 2, Y: 1}
	p := Point{X: 10, Y: 10}

	result := CheckPointInsideTriangle(a, b, c, p)

	if result {
		t.Errorf("Error asserting than the point is outside the triangle")
	}
}
