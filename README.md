# Vulcan Weather

El Repositorio consta de 3 aplicaciones,

las 3 se compilan con el bash build.sh

## cmd/predict/predict

Este es el ejercicio principal.

Salida de ejemplo.

```
$ predict
***** Vulcan Weather prediction system (VWPS) *****
In the next 10 years with 360 days, we expect:
Drought periods: 40
Rainy periods: 80
Rainy peak days: [72 252 288 432 612 648 792 972 1008 1152 1332 1368 1512 1692 1728 1872 2052 2088 2232 2412 2448 2592 2772 2808 2952 3132 3168 3312 3492 3528]
Optimal pressure and temperature conditions Periods: 120
***** Vulcan Weather prediction system (VWPS) *****
```

## cmd/loaddb/loaddb

este es el que carga los datos en la base de datos.
se usa una base de datos mysql.

la configuracion va por variables de entorno

```
$ DSN="root:weather@tcp(localhost:3306)/weather" ./loaddb
```

## cmd/api/api

Esta es la api rest. para iniciarla localmente se necesita la base precargada con la corrida del programa anterior

la configuracion va por variables de entorno

```
$ PORT="1234" DSN="root:weather@tcp(localhost:3306)/weather" ./api
```

la misma esta desplegada en app engine y esta accesible desde

$ curl https://meli-251021.appspot.com/


Los endpoints disponibles son

### GET /clima?dia=[int]

este devuelve el clima del dia

```
$ curl https://meli-251021.appspot.com/clima?dia=0
{"clima":"Sequia","dia":0}
```
### GET /clima?dia=[int]

este devuelve el clima del dia

```
$ curl https://meli-251021.appspot.com/clima?dia=0
{"clima":"Sequia","dia":0}
```

### [BONUS] GET /map?dia=[int]

este devuelve un svg con la posicion de los planetas



### [BONUS] GET /prediction

este devuelve lo mismo que el primer programa, pero en json


```
$ curl https://meli-251021.appspot.com/prediction
{"drought":40,"rainy":80,"rain_peaks":[72,252,288,432,612,648,792,972,1008,1152,1332,1368,1512,1692,1728,1872,2052,2088,2232,2412,2448,2592,2772,2808,2952,3132,3168,3312,3492,3528],"optimal":120}
```