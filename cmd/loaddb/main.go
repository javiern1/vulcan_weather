package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"gitlab.com/javiern1/vulcan_weather"
	_ "gitlab.com/javiern1/vulcan_weather/config"
	"log"
)

var daysInYear int
var years int
var dsn string

func init() {
	daysInYear = viper.GetInt("DAYS_IN_YEAR")
	years = viper.GetInt("YEARS")
	dsn = viper.GetString("DSN")
}

func main() {

	db, err := sql.Open("mysql", dsn)
	handleErr(err)

	err = db.Ping()
	handleErr(err)
	defer db.Close()

	log.Println("Cheking table existence")

	table, err := tableExists(db)
	handleErr(err)

	if table == "" {
		log.Println("Table does not exist, creating it")
		_, err = createTable(db)
		handleErr(err)
	} else {
		log.Println("Table exist, truncate it")
		_, err = truncateTable(db)
		handleErr(err)
	}

	err = populateDb(db)
	handleErr(err)

	log.Println("All done!")

}

func populateDb(db *sql.DB) (err error) {

	tx, err := db.Begin()
	handleErr(err)

	defer tx.Rollback()

	stmt, err := tx.Prepare("INSERT INTO predictions VALUES (null, ?, ?, ?, ?)")
	handleErr(err)
	defer stmt.Close()

	for x := 0; x < (years * daysInYear); x++ {
		var weather string
		weatherCode, perimeter := vulcan_weather.WeatherFor(x)

		switch weatherCode {
		case vulcan_weather.Drought:
			weather = "Sequia"
		case vulcan_weather.Optimal:
			weather = "Condiciones Optimas"
		case vulcan_weather.Rainy:
			weather = "Lluvia"
		default:
			weather = "Desconocido"
		}

		stmt.Exec(x, weatherCode, weather, perimeter)
		handleErr(err)
	}
	err = tx.Commit()
	return
}

func handleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func tableExists(db *sql.DB) (table string, err error) {
	rows, err := db.Query(`
SELECT TABLE_NAME
FROM information_schema.TABLES
WHERE TABLE_NAME = 'predictions'
	AND TABLE_SCHEMA = SCHEMA() 
	AND TABLE_NAME NOT LIKE '\_%'
	AND TABLE_NAME NOT LIKE '%\_xrefs'
`)
	handleErr(err)

	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&table)
		handleErr(err)
	}
	err = rows.Err()
	return
}

func createTable(db *sql.DB) (res sql.Result, err error) {
	res, err = db.Exec(`
create table predictions
(
	id int auto_increment primary key,
	day int not null,
	weather_code int not null,
	weather varchar (25) not null,
	perimeter decimal(25,15) not null default 0
)
`)
	return
}

func truncateTable(db *sql.DB) (res sql.Result, err error) {
	res, err = db.Exec(`truncate predictions`)
	return
}
