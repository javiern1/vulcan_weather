package main

import (
	"bytes"
	"gitlab.com/javiern1/vulcan_weather"
	"text/template"
)

const svg = `
<svg width="6000" height="6000" xmlns="http://www.w3.org/2000/svg">
    <!--<g>-->
        <!--<title>planets</title>-->
        <!--<rect fill="#fff" id="canvas_background" height="50002.18773" width="50002.18773" y="-1" x="-1"/>-->
        <!--<g display="none" id="canvasGrid"/>-->
    <!--</g>-->

    <g>
        <title>Sun</title>
        <circle cx="3000" cy="3000" r="100" stroke="black" stroke-width="3" fill="red" id="svg_sun"/>
    </g>

    <g>
        <title>Ferengi</title>
        <circle cx="3000" cy="3000" r="500" stroke="black" stroke-width="3" fill="none" id="svg_fere_o"/>

        <circle cx="{{add .Ferengi.X 3000}}" cy="{{add .Ferengi.Y 3000}}" r="40" stroke="black" stroke-width="3" fill="black" id="svg_fere"/>
    </g>

    <g>
        <title>Vulcano</title>
        <circle cx="3000" cy="3000" r="1000" stroke="black" stroke-width="3" fill="none" id="svg_vulcan_o"/>

        <circle cx="{{add .Vulcano.X 3000}}" cy="{{add .Vulcano.Y 3000}}" r="40" stroke="black" stroke-width="3" fill="green" id="svg_vulcan"/>
    </g>
    <g>
        <title>Betasoide</title>
        <circle cx="3000" cy="3000" r="2000" stroke="black" stroke-width="3" fill="none" id="svg_beta_o"/>

        <circle cx="{{add .Betasoide.X 3000}}" cy="{{add .Betasoide.Y 3000}}" r="40" stroke="black" stroke-width="3" fill="blue" id="svg_beta"/>
    </g>
</svg>
`

func GetSvg(day int, ferengi, vulcano, betasoide *vulcan_weather.Planet) ([]byte, error){

	type Coords struct {
		Ferengi, Vulcano, Betasoide vulcan_weather.Point
	}

	coords := Coords{
		Ferengi: ferengi.GetDegPosition(day),
		Vulcano: vulcano.GetDegPosition(day),
		Betasoide: betasoide.GetDegPosition(day),
	}

	funcMap := template.FuncMap{
		// The name "inc" is what the function will be called in the template text.
		"add": func(a,b float64, ) float64 {
			return a + b
		},
	}

	t := template.Must(template.New("svg").Funcs(funcMap).Parse(svg))
	buf := new(bytes.Buffer)
	err := t.Execute(buf, coords)
	return buf.Bytes(), err
}