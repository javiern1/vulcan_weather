package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/javiern1/vulcan_weather"
	_ "gitlab.com/javiern1/vulcan_weather/config"
	"log"
	"net/http"
	"strconv"
)

type WeatherRecord struct {
	Weather string `json:"clima"`
	Day     int    `json:"dia"`
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

var (
	port       string
	db         *sql.DB
	dsn        string
	daysInYear int
	years      int
)

func init() {
	var err error

	port = viper.GetString("PORT")
	daysInYear = viper.GetInt("DAYS_IN_YEAR")
	years = viper.GetInt("YEARS")
	dsn = viper.GetString("DSN")
	db, err = sql.Open("mysql", dsn)
	handleErr(err)

	err = db.Ping()
	handleErr(err)

}

func main() {
	defer db.Close()
	router := mux.NewRouter()
	router.HandleFunc("/clima", getWeather).Methods("GET")
	router.HandleFunc("/prediction", getPrediction).Methods("GET")
	router.HandleFunc("/map", drawSvg).Methods("GET")
	//log.Fatal(http.ListenAndServe(listen, router))
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func jsonError(w http.ResponseWriter, error string, code int) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(Error{Code: code, Message: error})
}

func drawSvg(w http.ResponseWriter, r *http.Request) {
	strDay := r.URL.Query().Get("dia")

	day, err := getDay(strDay)

	if err != nil {
		jsonError(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}

	svg, err := GetSvg(day,
		vulcan_weather.Planets["Ferengi"],
		vulcan_weather.Planets["Vulcano"],
		vulcan_weather.Planets["Betasoide"])

	if err != nil {
		jsonError(w, "500 internal server error", http.StatusInternalServerError)
		log.Println(err)
		return
	}

	w.Header().Set("Content-Type", "image/svg+xml; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write(svg)
}

func getPrediction(w http.ResponseWriter, r *http.Request) {
	prediction := vulcan_weather.Predict(years * daysInYear)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(prediction)
}

func getWeather(w http.ResponseWriter, r *http.Request) {
	strDay := r.URL.Query().Get("dia")

	day, err := getDay(strDay)

	if err != nil {
		jsonError(w, fmt.Sprint(err), http.StatusBadRequest)
		return
	}

	weather, err := queryDatabase(db, day)
	if err != nil {
		jsonError(w, "500 internal server error", http.StatusInternalServerError)
		log.Println(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(weather)
}

func queryDatabase(db *sql.DB, day int) (res WeatherRecord, err error) {
	err = db.QueryRow("SELECT day, weather FROM predictions WHERE day = ?", day).Scan(&res.Day, &res.Weather)
	return
}

func handleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func getDay(strDay string) (day int, err error) {
	day, err = strconv.Atoi(strDay)
	if err != nil {
		return day, errors.New("You have to send a number")
	}

	if day < 0 || day > (daysInYear*years-1) {
		return day, errors.New("Number is out of range")
	}

	return
}