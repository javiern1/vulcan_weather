package main

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/javiern1/vulcan_weather"
	_ "gitlab.com/javiern1/vulcan_weather/config"
	"os"
)

var daysInYear int
var years int

func init() {
	daysInYear = viper.GetInt("DAYS_IN_YEAR")
	years = viper.GetInt("YEARS")
}

func main() {
	prediction := vulcan_weather.Predict(years * daysInYear)

	fmt.Printf("***** Vulcan Weather prediction system (VWPS) *****\n")
	fmt.Printf("In the next %d years with %d days, we expect:\n", years, daysInYear)
	fmt.Printf("Drought periods: %d\n", prediction.Drought)
	fmt.Printf("Rainy periods: %d\n", prediction.Rainy)
	fmt.Printf("Rainy peak days: %v\n", prediction.RainPeaks)
	fmt.Printf("Optimal pressure and temperature conditions Periods: %d\n", prediction.Optimal)
	fmt.Printf("***** Vulcan Weather prediction system (VWPS) *****\n")

	os.Exit(0)
}