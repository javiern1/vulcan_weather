#!/usr/bin/env bash
CWD=$(pwd)
go build ${CWD}/cmd/api
go build ${CWD}/cmd/loaddb
go build ${CWD}/cmd/predict
go test -coverprofile cp.out ./...